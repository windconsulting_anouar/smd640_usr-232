#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "SDM630.h"
#include "SDM630_regs.h"



void parseReqFrameTCPModbus(uint8_t slaveId, uint8_t highByte, uint8_t lowByte, uint8_t regNbr)
{
    memset(reqFrame, 0x00, 12);
    reqFrame[0]    = slaveId;
    reqFrame[1]    = 4;       //function code
    reqFrame[2]    = 6;       //size
    reqFrame[6]    = slaveId;
    reqFrame[7]    = 4;       //function code
    reqFrame[8]    = highByte;
    reqFrame[9]    = lowByte;
    reqFrame[11]   = regNbr;
}


float bytesToFloat(uint8_t * bytes)
{
    uint8_t arr[4] = {bytes[3], bytes[2], bytes[1], bytes[0]};
    return *(float*)arr;
}


void readMesure(int client_fd, mesure_t * mesure)
{
    /************************  read 42 Registers adresses from 30001 to 30041 ************************/
    parseReqFrameTCPModbus(2, PH1_VOLTAGE_H, PH1_VOLTAGE_L, 0x2A);
    int n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    float * regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 84);
    mesure->ph1_voltage = bytesToFloat((uint8_t*)regsData);
    mesure->ph2_voltage = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_voltage = bytesToFloat((uint8_t*)++regsData);

    mesure->ph1_current = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_current = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_current = bytesToFloat((uint8_t*)++regsData);

    mesure->ph1_power = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_power = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_power = bytesToFloat((uint8_t*)++regsData);

    mesure->ph1_volt_amp = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_volt_amp = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_volt_amp = bytesToFloat((uint8_t*)++regsData);

    mesure->ph1_react_power = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_react_power = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_react_power = bytesToFloat((uint8_t*)++regsData);

    mesure->ph1_power_fact = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_power_fact = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_power_fact = bytesToFloat((uint8_t*)++regsData);
            
    mesure->ph1_phase_angl = bytesToFloat((uint8_t*)++regsData);
    mesure->ph2_phase_angl = bytesToFloat((uint8_t*)++regsData);
    mesure->ph3_phase_angl = bytesToFloat((uint8_t*)++regsData);


    /************************ read 46 Registers adresses from 30043 to 30087 ************************/
    //memset(resFrame, 0x00, 200);
    parseReqFrameTCPModbus(2, AVG_LINE_VOLTAGE_H, AVG_LINE_VOLTAGE_L, 0x2E);
    n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 92);
    mesure->avg_line_voltage = bytesToFloat((uint8_t*)regsData);            // 0x00 0x2A
    mesure->avg_line_current = bytesToFloat((uint8_t*)(regsData += 2));     // 0x00 0x2E
    mesure->sum_line_current = bytesToFloat((uint8_t*)++regsData);          // 0x00 0x30
    mesure->tot_sys_power = bytesToFloat((uint8_t*)(regsData += 2));        // 0x00 0x34
    mesure->tot_sys_volt_amp = bytesToFloat((uint8_t*)(regsData += 2));     // 0x00 0x38
    mesure->tot_sys_react_pow = bytesToFloat((uint8_t*)(regsData += 2));    // 0x00 0x3C
    mesure->tot_sys_pow_fact = bytesToFloat((uint8_t*)++regsData);          // 0x00 0x3E
    mesure->tot_sys_phase_ang = bytesToFloat((uint8_t*)(regsData += 2));    // 0x00 0x42
    mesure->freq_supply_volt = bytesToFloat((uint8_t*)(regsData += 2));     // 0x00 0x46
    mesure->imp_wh_last_reset = bytesToFloat((uint8_t*)++regsData);         // 0x00 0x48
    mesure->exp_wh_last_reset = bytesToFloat((uint8_t*)++regsData);         // 0x00 0x4A
    mesure->imp_varh_last_reset = bytesToFloat((uint8_t*)++regsData);       // 0x00 0x4C
    mesure->exp_varh_last_reset = bytesToFloat((uint8_t*)++regsData);       // 0x00 0x4E
    mesure->vah_last_reset = bytesToFloat((uint8_t*)++regsData);            // 0x00 0x50     
    mesure->va_last_reset = bytesToFloat((uint8_t*)++regsData);             // 0x00 0x52
    mesure->tot_sys_pow_demand = bytesToFloat((uint8_t*)++regsData);        // 0x00 0x54
    mesure->max_sys_pow_demand = bytesToFloat((uint8_t*)++regsData);        // 0x00 0x56


    /************************ read 8 Registers adresses from 30101 to 30107 ************************/
    //memset(resFrame, 0x00, 200);
    parseReqFrameTCPModbus(2, TOT_SYS_VA_DEMAND_H, TOT_SYS_VA_DEMAND_L, 0x08);
    n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 16);
    mesure->tot_sys_va_demand = bytesToFloat((uint8_t*)regsData);          // 0x00 0x64
    mesure->max_sys_va_demand = bytesToFloat((uint8_t*)++regsData);        // 0x00 0x66
    mesure->neutr_curr_demand = bytesToFloat((uint8_t*)++regsData);        // 0x00 0x68
    mesure->max_neutr_curr_demand = bytesToFloat((uint8_t*)++regsData);    // 0x00 0x6A


    /************************ read 8 Registers adresses from 30201 to 30207 ***********************/
    //memset(resFrame, 0x00, 200);
    parseReqFrameTCPModbus(2, LINE_1_TO_2_VOLTAGE_H, LINE_1_TO_2_VOLTAGE_L, 0x08);
    n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 16);
    mesure->line_1_to_2_voltage = bytesToFloat((uint8_t*)regsData);          // 0x00 0xC8
    mesure->line_2_to_3_voltage = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xCA
    mesure->line_3_to_1_voltage = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xCC
    mesure->avg_line_to_line_volt = bytesToFloat((uint8_t*)++regsData);      // 0x00 0xCe


    /************************ read 46 Registers adresses from 30225 to 30269 ***********************/
    //memset(resFrame, 0x00, 200);
    parseReqFrameTCPModbus(2, NEUTRAL_CURRENT_H, NEUTRAL_CURRENT_L, 0x2E);
    n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 92);
    mesure->neutral_current = bytesToFloat((uint8_t*)regsData);          // 0x00 0xE0
    mesure->ph1_ln_volt_thd = bytesToFloat((uint8_t*)(regsData += 5));   // 0x00 0xEA
    mesure->ph2_ln_volt_thd = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xEC
    mesure->ph3_ln_volt_thd = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xEE
    mesure->ph1_current_thd = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xF0
    mesure->ph2_current_thd = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xF2
    mesure->ph3_current_thd = bytesToFloat((uint8_t*)++regsData);        // 0x00 0xF4
    mesure->avg_line_neutr_volt = bytesToFloat((uint8_t*)(regsData += 2));// 0x00 0xF8
    mesure->avg_line_current_thd = bytesToFloat((uint8_t*)++regsData);   // 0x00 0xFA
    mesure->ph1_current_demand = bytesToFloat((uint8_t*)++regsData);     // 0x00 0x02
    mesure->ph2_current_demand = bytesToFloat((uint8_t*)++regsData);     // 0x00 0x04
    mesure->ph3_current_demand = bytesToFloat((uint8_t*)++regsData);     // 0x00 0x06
    mesure->ph1_max_curr_demand = bytesToFloat((uint8_t*)++regsData);    // 0x00 0x08
    mesure->ph2_max_curr_demand = bytesToFloat((uint8_t*)++regsData);    // 0x00 0x0A
    mesure->ph3_max_curr_demand = bytesToFloat((uint8_t*)++regsData);    // 0x00 0x0C


    /************************ read 12 Registers adresses from 30335 to 30345 ***********************/
    //memset(resFrame, 0x00, 200);
    parseReqFrameTCPModbus(2, LINE_1_TO_2_VOLT_THD_H, LINE_1_TO_2_VOLT_THD_L, 0x0C);
    n = write(client_fd, reqFrame, sizeof(reqFrame));

    if (n < 0){
            printf("ERROR writing to socket\n");
    }

    n = read(client_fd, resFrame, sizeof(resFrame));

    if (n < 0){
        printf("ERROR reading from socket\n");
    }

    regsData = (float*)&resFrame[9];
    //printBytesArray((uint8_t*)regsData, 24);
    mesure->line_1_to_2_volt_thd = bytesToFloat((uint8_t*)regsData);            // 0x01 0x4E
    mesure->line_2_to_3_volt_thd = bytesToFloat((uint8_t*)++regsData);          // 0x01 0x50
    mesure->line_3_to_1_volt_thd = bytesToFloat((uint8_t*)++regsData);          // 0x01 0x52
    mesure->avg_line_to_line_volt_thd  = bytesToFloat((uint8_t*)++regsData);    // 0x01 0x54
    mesure->total_khw = bytesToFloat((uint8_t*)++regsData);                     // 0x01 0x56
    mesure->total_kvarh = bytesToFloat((uint8_t*)++regsData);                   // 0x01 0x58

}


void printBytesArray(uint8_t * arr, uint8_t size)
{
    int i ;
    for (i = 0; i < size; i++)
    {
        printf("%02x ", arr[i]);
    }

    printf("\n");
}


