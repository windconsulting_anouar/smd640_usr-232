#ifndef __SDM630_REGS_H
#define __SDM630_REGS_H


#define PH1_VOLTAGE_H			0x00 		// REG_ADDR=30001	Phase 1 line to neutral volts [Volts]
#define PH1_VOLTAGE_L			0x00 		// REG_ADDR=30001	Phase 1 line to neutral volts [Volts]
#define PH2_VOLTAGE_H			0x00 		// REG_ADDR=30003	Phase 2 line to neutral volts [Volts]
#define PH2_VOLTAGE_L			0x02 		// REG_ADDR=30003	Phase 2 line to neutral volts [Volts]
#define PH3_VOLTAGE_H			0x00 		// REG_ADDR=30005	Phase 3 line to neutral volts [Volts]
#define PH3_VOLTAGE_L			0x04 		// REG_ADDR=30005	Phase 3 line to neutral volts [Volts]

#define PH1_CURRENT_H			0x00 		// REG_ADDR=30007	Phase 1 current [Amps]
#define PH1_CURRENT_L			0x06 		// REG_ADDR=30007	Phase 1 current [Amps]
#define PH2_CURRENT_H			0x00 		// REG_ADDR=30009	Phase 2 current [Amps]
#define PH2_CURRENT_L			0x08 		// REG_ADDR=30009	Phase 2 current [Amps]
#define PH3_CURRENT_H			0x00 		// REG_ADDR=30011	Phase 3 current [Amps]
#define PH3_CURRENT_L			0x0A 		// REG_ADDR=30011	Phase 3 current [Amps]

#define PH1_POWER_H				0x00 		// REG_ADDR=30013	Phase 1 power [Watts]
#define PH1_POWER_L				0x0C 		// REG_ADDR=30013	Phase 1 power [Watts]
#define PH2_POWER_H				0x00 		// REG_ADDR=30015	Phase 2 power [Watts]
#define PH2_POWER_L				0x0E 		// REG_ADDR=30015	Phase 2 power [Watts]
#define PH3_POWER_H				0x00 		// REG_ADDR=30017	Phase 3 power [Watts]
#define PH3_POWER_L				0x10 		// REG_ADDR=30017	Phase 3 power [Watts]

#define PH1_VOLT_AMP_H			0x00 		// REG_ADDR=30019	Phase 1 volt amps [VA]
#define PH1_VOLT_AMP_L			0x12 		// REG_ADDR=30019	Phase 1 volt amps [VA]
#define PH2_VOLT_AMP_H			0x00 		// REG_ADDR=30021	Phase 2 volt amps [VA]
#define PH2_VOLT_AMP_L			0x14 		// REG_ADDR=30021	Phase 2 volt amps [VA]
#define PH3_VOLT_AMP_H			0x00 		// REG_ADDR=30023	Phase 3 volt amps [VA]
#define PH3_VOLT_AMP_L			0x16 		// REG_ADDR=30023	Phase 3 volt amps [VA]

#define PH1_REACT_POWER_H		0x00 		// REG_ADDR=30025	Phase 1 volt amps reactive [VAr]
#define PH1_REACT_POWER_L		0x18 		// REG_ADDR=30025	Phase 1 volt amps reactive [VAr]
#define PH2_REACT_POWER_H		0x00 		// REG_ADDR=30027	Phase 2 volt amps reactive [VAr]
#define PH2_REACT_POWER_L		0x1A 		// REG_ADDR=30027	Phase 2 volt amps reactive [VAr]
#define PH3_REACT_POWER_H		0x00 		// REG_ADDR=30029	Phase 3 volt amps reactive [VAr]
#define PH3_REACT_POWER_L		0x1C 		// REG_ADDR=30029	Phase 3 volt amps reactive [VAr]

#define PH1_POWER_FACT_H		0x00 		// REG_ADDR=30031	Phase 1 power factor (1) [None]
#define PH1_POWER_FACT_L		0x1E 		// REG_ADDR=30031	Phase 1 power factor (1) [None]
#define PH2_POWER_FACT_H		0x00 		// REG_ADDR=30033	Phase 2 power factor (1) [None]
#define PH2_POWER_FACT_L		0x20 		// REG_ADDR=30033	Phase 2 power factor (1) [None]
#define PH3_POWER_FACT_H		0x00 		// REG_ADDR=30035	Phase 3 power factor (1) [None]
#define PH3_POWER_FACT_L		0x22 		// REG_ADDR=30035	Phase 3 power factor (1) [None]
		
#define PH1_PHASE_ANGL_H		0x00 		// REG_ADDR=30037	Phase 1 phase angle [Degre es]
#define PH1_PHASE_ANGL_L		0x24 		// REG_ADDR=30037	Phase 1 phase angle [Degre es]
#define PH2_PHASE_ANGL_H		0x00 		// REG_ADDR=30039	Phase 2 phase angle [Degre es]
#define PH2_PHASE_ANGL_L		0x26 		// REG_ADDR=30039	Phase 2 phase angle [Degre es]
#define PH3_PHASE_ANGL_H		0x00 		// REG_ADDR=30041	Phase 3 phase angle [Degre es]
#define PH3_PHASE_ANGL_L		0x28 		// REG_ADDR=30041	Phase 3 phase angle [Degre es]

#define AVG_LINE_VOLTAGE_H		0x00 		// REG_ADDR=30043	Average line to neutral volts [Volts]
#define AVG_LINE_VOLTAGE_L		0x2A 		// REG_ADDR=30043	Average line to neutral volts [Volts]

#define AVG_LINE_CURRENT_H		0x00 		// REG_ADDR=30047	Average line current [Amps]
#define AVG_LINE_CURRENT_L		0x28 		// REG_ADDR=30047	Average line current [Amps]

#define SUM_LINE_CURRENT_H		0x00 		// REG_ADDR=30049	Sum of line currents [Amps]
#define SUM_LINE_CURRENT_L		0x30 		// REG_ADDR=30049	Sum of line currents [Amps]

#define TOT_SYS_POWER_H			0x00 		// REG_ADDR=30053	Total system power [Watts]
#define TOT_SYS_POWER_L			0x34 		// REG_ADDR=30053	Total system power [Watts]

#define TOT_SYS_VOLT_AMP_H		0x00 		// REG_ADDR=30057	Total system volt amps [VA]
#define TOT_SYS_VOLT_AMP_L		0x38 		// REG_ADDR=30057	Total system volt amps [VA]
	
#define TOT_SYS_REACT_POW_H		0x00 		// REG_ADDR=30061	Total system VAr [VAr]
#define TOT_SYS_REACT_POW_L		0x3C 		// REG_ADDR=30061	Total system VAr [VAr]
		
#define TOT_SYS_POW_FACT_H		0x00 		// REG_ADDR=30063	Total system power factor (1) [None]
#define TOT_SYS_POW_FACT_L		0x3E 		// REG_ADDR=30063	Total system power factor (1) [None]
	
#define TOT_SYS_PHASE_ANG_H		0x00 		// REG_ADDR=30067	Total system phase angle [Degre es]
#define TOT_SYS_PHASE_ANG_L		0x42 		// REG_ADDR=30067	Total system phase angle [Degre es]

#define FREQ_SUPPLY_VOLT_H		0x00 		// REG_ADDR=30071	Frequency of supply voltages [Hz]
#define FREQ_SUPPLY_VOLT_L		0x46 		// REG_ADDR=30071	Frequency of supply voltages [Hz]

#define IMP_WH_LAST_RESET_H		0x00 		// REG_ADDR=30073	Import Wh since last reset (2) [kWh/M Wh]
#define IMP_WH_LAST_RESET_L		0x48 		// REG_ADDR=30073	Import Wh since last reset (2) [kWh/M Wh]

#define EXP_WH_LAST_RESET_H		0x00 		// REG_ADDR=30075	Export Wh since last reset (2) [kWH/ MWh]
#define EXP_WH_LAST_RESET_L		0x4A 		// REG_ADDR=30075	Export Wh since last reset (2) [kWH/ MWh]
	
#define IMP_VARH_LAST_RESET_H	0x00 		// REG_ADDR=30077	Import VArh since last reset (2) [kVArh/MVArh]
#define IMP_VARH_LAST_RESET_L	0x4C 		// REG_ADDR=30077	Import VArh since last reset (2) [kVArh/MVArh]

#define EXP_VARH_LAST_RESET_H	0x00 		// REG_ADDR=30079	Export VArh since last reset (2) [kVArh/MVArh]
#define EXP_VARH_LAST_RESET_L	0x4E 		// REG_ADDR=30079	Export VArh since last reset (2) [kVArh/MVArh]

#define VAH_LAST_RESET_H		0x00 		// REG_ADDR=30081	VAh since last reset (2) [kVAh/MVAh]
#define VAH_LAST_RESET_L		0x50 		// REG_ADDR=30081	VAh since last reset (2) [kVAh/MVAh]

#define VA_LAST_RESET_H			0x00 		// REG_ADDR=30083	Ah since last reset(3) [Ah/kAh]
#define VA_LAST_RESET_L			0x52 		// REG_ADDR=30083	Ah since last reset(3) [Ah/kAh]
	
#define TOT_SYS_POW_DEMAND_H	0x00 		// REG_ADDR=30085	Total system power demand [W]
#define TOT_SYS_POW_DEMAND_L	0x54 		// REG_ADDR=30085	Total system power demand [W]
	
#define MAX_SYS_POW_DEMAND_H	0x00 		// REG_ADDR=30087	Maximum total system power demand [VA]
#define MAX_SYS_POW_DEMAND_L	0x56 		// REG_ADDR=30087	Maximum total system power demand [VA]
		
#define TOT_SYS_VA_DEMAND_H		0x00 		// REG_ADDR=30101	Total system VA demand [VA]
#define TOT_SYS_VA_DEMAND_L		0x64 		// REG_ADDR=30101	Total system VA demand [VA]

#define MAX_SYS_VA_DEMAND_H		0x00 		// REG_ADDR=30103	Maximum total  system VA demand [VA]
#define MAX_SYS_VA_DEMAND_L		0x66 		// REG_ADDR=30103	Maximum total  system VA demand [VA]

#define NEUTR_CURR_DEMAND_H		0x00 		// REG_ADDR=30105	Neutral current demand [Amps]
#define NEUTR_CURR_DEMAND_L		0x68 		// REG_ADDR=30105	Neutral current demand [Amps]
	
#define MAX_NEUTR_CURR_DEMAND_H	0x00 		// REG_ADDR=30107	Maximum  neutral  current demand [Amps]
#define MAX_NEUTR_CURR_DEMAND_L	0x6A 		// REG_ADDR=30107	Maximum  neutral  current demand [Amps]

#define LINE_1_TO_2_VOLTAGE_H	0x00 		// REG_ADDR=30201	Line 1 to Line 2 volts [Volts]
#define LINE_1_TO_2_VOLTAGE_L	0xC8 		// REG_ADDR=30201	Line 1 to Line 2 volts [Volts]
#define LINE_2_TO_3_VOLTAGE_H	0x00 		// REG_ADDR=30203	Line 2 to Line 3 volts [Volts]
#define LINE_2_TO_3_VOLTAGE_L	0xCA 		// REG_ADDR=30203	Line 2 to Line 3 volts [Volts]
#define LINE_3_TO_1_VOLTAGE_H	0x00 		// REG_ADDR=30205	Line 3 to Line 1 volts [Volts]
#define LINE_3_TO_1_VOLTAGE_L	0xCC 		// REG_ADDR=30205	Line 3 to Line 1 volts [Volts]

#define AVG_LINE_TO_LINE_VOLT_H	0x00 		// REG_ADDR=30207	Average line to line volts [Volts] 
#define AVG_LINE_TO_LINE_VOLT_L	0xCE 		// REG_ADDR=30207	Average line to line volts [Volts] 

#define NEUTRAL_CURRENT_H		0x00 		// REG_ADDR=30225	Neutral current [Amps]
#define NEUTRAL_CURRENT_L		0xE0 		// REG_ADDR=30225	Neutral current [Amps]

#define PH1_LN_VOLT_THD_H		0x00 		// REG_ADDR=30235	Phase 1 L/N volts THD [%]
#define PH1_LN_VOLT_THD_L		0xEA 		// REG_ADDR=30235	Phase 1 L/N volts THD [%]
#define PH2_LN_VOLT_THD_H		0x00 		// REG_ADDR=30237	Phase 2 L/N volts THD [%]
#define PH2_LN_VOLT_THD_L		0xEC 		// REG_ADDR=30237	Phase 2 L/N volts THD [%]
#define PH3_LN_VOLT_THD_H		0x00 		// REG_ADDR=30239	Phase 3 L/N volts THD [%]
#define PH3_LN_VOLT_THD_L		0xEE 		// REG_ADDR=30239	Phase 3 L/N volts THD [%]

#define PH1_CURRENT_THD_H		0x00 		// REG_ADDR=30241	Phase 1 Current THD [%]
#define PH1_CURRENT_THD_L		0xF0 		// REG_ADDR=30241	Phase 1 Current THD [%]
#define PH2_CURRENT_THD_H		0x00 		// REG_ADDR=30243	Phase 2 Current THD [%]
#define PH2_CURRENT_THD_L		0xF2 		// REG_ADDR=30243	Phase 2 Current THD [%]
#define PH3_CURRENT_THD_H		0x00 		// REG_ADDR=30245	Phase 3 Current THD [%]
#define PH3_CURRENT_THD_L		0xF4 		// REG_ADDR=30245	Phase 3 Current THD [%] 

#define AVG_LINE_NEUTR_VOLT_H	0x00 		// REG_ADDR=30249	Average line to neutral volts THD [%]
#define AVG_LINE_NEUTR_VOLT_L	0xF8 		// REG_ADDR=30249	Average line to neutral volts THD [%]

#define AVG_LINE_CURRENT_THD_H	0x00 		// REG_ADDR=30251	Average line current THD [%]
#define AVG_LINE_CURRENT_THD_L	0xFA 		// REG_ADDR=30251	Average line current THD [%]

#define PH1_CURRENT_DEMAND_H	0x01 		// REG_ADDR=30259	Phase 1 current demand [Amps]
#define PH1_CURRENT_DEMAND_L	0x02 		// REG_ADDR=30259	Phase 1 current demand [Amps]
#define PH2_CURRENT_DEMAND_H	0x01 		// REG_ADDR=30261	Phase 2 current demand [Amps]
#define PH2_CURRENT_DEMAND_L	0x04 		// REG_ADDR=30261	Phase 2 current demand [Amps]
#define PH3_CURRENT_DEMAND_H	0x01 		// REG_ADDR=30263	Phase 3 current demand [Amps]
#define PH3_CURRENT_DEMAND_L	0x06 		// REG_ADDR=30263	Phase 3 current demand [Amps]

#define PH1_MAX_CURR_DEMAND_H	0x01 		// REG_ADDR=30265	Maximum phase  1  current demand [Amps]
#define PH1_MAX_CURR_DEMAND_L	0x08 		// REG_ADDR=30265	Maximum phase  1  current demand [Amps]
#define PH2_MAX_CURR_DEMAND_H	0x01 		// REG_ADDR=30267	Maximum phase  2  current demand [Amps]
#define PH2_MAX_CURR_DEMAND_L	0x0A 		// REG_ADDR=30267	Maximum phase  2  current demand [Amps]
#define PH3_MAX_CURR_DEMAND_H	0x01 		// REG_ADDR=30269	Maximum phase  3  current demand [Amps]
#define PH3_MAX_CURR_DEMAND_L	0x0C 		// REG_ADDR=30269	Maximum phase  3  current demand [Amps]

#define LINE_1_TO_2_VOLT_THD_H	0x01 		// REG_ADDR=30335	Line 1 to line 2 volts THD [％]
#define LINE_1_TO_2_VOLT_THD_L	0x4E 		// REG_ADDR=30335	Line 1 to line 2 volts THD [％]
#define LINE_2_TO_3_VOLT_THD_H	0x01 		// REG_ADDR=30337	Line 2 to line 3 volts THD [％]
#define LINE_2_TO_3_VOLT_THD_L	0x50 		// REG_ADDR=30337	Line 2 to line 3 volts THD [％]
#define LINE_3_TO_1_VOLT_THD_H	0x01 		// REG_ADDR=30339	Line 3 to line 1 volts THD [％]
#define LINE_3_TO_1_VOLT_THD_L	0x52 		// REG_ADDR=30339	Line 3 to line 1 volts THD [％]

#define AVG_LINE_TO_LINE_VOLT_THD_H	0x01 	// REG_ADDR=30341	Average line to line volts THD [％]
#define AVG_LINE_TO_LINE_VOLT_THD_L	0x54 	// REG_ADDR=30341	Average line to line volts THD [％]

#define TOTAL_KHW_H				0x01		// REG_ADDR=30343	Total kwh [kwh]
#define TOTAL_KHW_L				0x56		// REG_ADDR=30343	Total kwh [kwh]

#define TOTAL_KVARH_H			0x01		// REG_ADDR=30345	Total kvarh [kvarh]
#define TOTAL_KVARH_L			0x58		// REG_ADDR=30345	Total kvarh [kvarh]


#endif //__SDM630_REGS_H