CFLAGS = -c -g
CC = gcc ${DEFINE}

all : clean out

out : SDM630.o main.o
        $(CC) -g -o $@ $^ -lconfig

SDM630.o : SDM630.c SDM630.h SDM630_regs.h
        $(CC) $(CFLAGS) -o $@ $<

main.o : main.c
        $(CC) $(CFLAGS) -o $@ $<

clean :
        rm -f *.o out
