#include <libconfig.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>

#include "SDM630.h"

#define CONFIG_FILENAME     "config.cfg"

static FILE * pMesureFile;
static char mesureFilename[30];
static char mesureRow[500];

void saveData (mesure_t * mesure);
int readConfig(char * hostname, int * port,int * delay);

int main()
{
    char hostname[50];
    int port;
    int delay;
    if(readConfig(hostname, &port, &delay))
    {
        fprintf(stdout, "Error read configuration file\n");
    }

    int client_fd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    client_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (client_fd < 0)
        printf("ERROR opening socket\n");

    server = gethostbyname(hostname);

    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    memset (&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(port);

    if (connect(client_fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        printf("ERROR connecting\n");

    time_t timestamp = time(NULL);            
    sprintf(mesureFilename, "./mesure_%ld.txt", timestamp);

   
    while (1)
    {
        mesure_t mesure;
        readMesure(client_fd, &mesure);

        printf("ph1_voltage = %f\n", mesure.ph1_voltage);
        printf("ph2_voltage = %f\n", mesure.ph2_voltage);
        printf("ph3_voltage = %f\n", mesure.ph3_voltage);
        printf("ph1_current = %f\n", mesure.ph1_current);
        printf("ph2_current = %f\n", mesure.ph2_current);
        printf("ph3_current = %f\n", mesure.ph3_current);
        printf("ph1_power = %f\n", mesure.ph1_power);
        printf("ph2_power = %f\n", mesure.ph2_power);
        printf("ph3_power = %f\n", mesure.ph3_power);
        printf("ph1_volt_amp = %f\n", mesure.ph1_volt_amp);
        printf("ph2_volt_amp = %f\n", mesure.ph2_volt_amp);
        printf("ph3_volt_amp = %f\n", mesure.ph3_volt_amp);
        printf("ph1_react_power = %f\n", mesure.ph1_react_power);
        printf("ph2_react_power = %f\n", mesure.ph2_react_power);
        printf("ph3_react_power = %f\n", mesure.ph3_react_power);
        printf("ph1_power_fact = %f\n", mesure.ph1_power_fact);
        printf("ph2_power_fact = %f\n", mesure.ph2_power_fact);
        printf("ph3_power_fact = %f\n", mesure.ph3_power_fact);
        printf("ph1_phase_angl = %f\n", mesure.ph1_phase_angl);
        printf("ph2_phase_angl = %f\n", mesure.ph2_phase_angl);
        printf("ph3_phase_angl = %f\n", mesure.ph3_phase_angl);

        printf("avg_line_voltage = %f\n", mesure.avg_line_voltage); 
        printf("avg_line_current = %f\n", mesure.avg_line_current); 
        printf("sum_line_current = %f\n", mesure.sum_line_current); 
        printf("tot_sys_power = %f\n", mesure.tot_sys_power); 
        printf("tot_sys_volt_amp = %f\n", mesure.tot_sys_volt_amp); 
        printf("tot_sys_react_pow = %f\n", mesure.tot_sys_react_pow); 
        printf("tot_sys_pow_fact = %f\n", mesure.tot_sys_pow_fact); 
        printf("tot_sys_phase_ang = %f\n", mesure.tot_sys_phase_ang); 
        printf("freq_supply_volt = %f\n", mesure.freq_supply_volt); 
        printf("imp_wh_last_reset = %f\n", mesure.imp_wh_last_reset); 
        printf("exp_wh_last_reset = %f\n", mesure.exp_wh_last_reset); 
        printf("imp_varh_last_reset = %f\n", mesure.imp_varh_last_reset); 
        printf("exp_varh_last_reset = %f\n", mesure.exp_varh_last_reset); 
        printf("vah_last_reset = %f\n", mesure.vah_last_reset); 
        printf("va_last_reset = %f\n", mesure.va_last_reset); 
        printf("tot_sys_pow_demand = %f\n", mesure.tot_sys_pow_demand); 
        printf("max_sys_pow_demand = %f\n\n", mesure.max_sys_pow_demand);

        printf("tot_sys_va_demand = %f\n", mesure.tot_sys_va_demand);
        printf("max_sys_va_demand = %f\n", mesure.max_sys_va_demand);
        printf("neutr_curr_demand = %f\n", mesure.neutr_curr_demand);
        printf("max_neutr_curr_demand = %f\n\n", mesure.max_neutr_curr_demand);

        printf("line_1_to_2_voltage = %f\n", mesure.line_1_to_2_voltage);
        printf("line_2_to_3_voltage = %f\n", mesure.line_2_to_3_voltage);
        printf("line_3_to_1_voltage = %f\n", mesure.line_3_to_1_voltage);
        printf("avg_line_to_line_volt = %f\n\n", mesure.avg_line_to_line_volt);

        printf("neutral_current = %f\n", mesure.neutral_current);
        printf("ph1_ln_volt_thd = %f\n", mesure.ph1_ln_volt_thd);
        printf("ph2_ln_volt_thd = %f\n", mesure.ph2_ln_volt_thd);
        printf("ph3_ln_volt_thd = %f\n", mesure.ph3_ln_volt_thd);
        printf("ph1_current_thd = %f\n", mesure.ph1_current_thd);
        printf("ph2_current_thd = %f\n", mesure.ph2_current_thd);
        printf("ph3_current_thd = %f\n", mesure.ph3_current_thd);
        printf("avg_line_neutr_volt = %f\n", mesure.avg_line_neutr_volt);
        printf("avg_line_current_thd = %f\n", mesure.avg_line_current_thd);
        printf("ph1_current_demand = %f\n", mesure.ph1_current_demand);
        printf("ph2_current_demand = %f\n", mesure.ph2_current_demand);
        printf("ph3_current_demand = %f\n", mesure.ph3_current_demand);
        printf("ph1_max_curr_demand = %f\n", mesure.ph1_max_curr_demand);
        printf("ph2_max_curr_demand = %f\n", mesure.ph2_max_curr_demand);
        printf("ph3_max_curr_demand = %f\n\n", mesure.ph3_max_curr_demand);

        printf("line_1_to_2_volt_thd = %f\n", mesure.line_1_to_2_volt_thd);
        printf("line_2_to_3_volt_thd = %f\n", mesure.line_2_to_3_volt_thd);
        printf("line_3_to_1_volt_thd = %f\n", mesure.line_3_to_1_volt_thd);
        printf("avg_line_to_line_volt_thd = %f\n", mesure.avg_line_to_line_volt_thd );
        printf("total_khw = %f\n", mesure.total_khw);
        printf("total_kvarh = %f\n\n", mesure.total_kvarh);
        printf("----------------------------------------------------------\n");
        
        saveData (&mesure);
        sleep(delay);
    }

    return 0;
}

int readConfig(char * hostname, int * port, int * delay)
{
    config_t cfg;
    const char * l_hostname = NULL;
    const char * l_port = NULL;
    const char * l_delay = NULL;

    config_init(&cfg);
	
	if (!config_read_file(&cfg, CONFIG_FILENAME)) {
		fprintf(stdout, "Configuration file not found!");
		config_destroy(&cfg);
		return -1;
	}

	if (!config_lookup_string(&cfg, "HOSTNAME", &l_hostname)) {
		fprintf(stdout, "No 'hostname' setting in configuration file.\r\n");
		config_destroy(&cfg);
		return -1;
	}

    if (!config_lookup_string(&cfg, "PORT", &l_port)) {
		fprintf(stdout, "No 'port' setting in configuration file.\r\n");
		config_destroy(&cfg);
		return -1;
	}
	
    if (!config_lookup_string(&cfg, "DELAY", &l_delay)) {
		fprintf(stdout, "No 'delay' setting in configuration file.\r\n");
		config_destroy(&cfg);
		return -1;
	}
    
    strcpy(hostname, l_hostname);
    *port = atoi(l_port);
    *delay=atoi(l_delay);
    config_destroy(&cfg);

    return 0;
}

void saveData (mesure_t * mesure)
{
    sprintf (mesureRow, "%lu;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;"
                                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;"
                                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f\n",
                                                                    (unsigned long)time(NULL),
                                                                    mesure->ph1_voltage,
                                                                    mesure->ph2_voltage,
                                                                    mesure->ph3_voltage,
                                                                    mesure->ph1_current,
                                                                    mesure->ph2_current,
                                                                    mesure->ph3_current,
                                                                    mesure->ph1_power,
                                                                    mesure->ph2_power,
                                                                    mesure->ph3_power,
                                                                    mesure->ph1_volt_amp,
                                                                    mesure->ph2_volt_amp,
                                                                    mesure->ph3_volt_amp,
                                                                    mesure->ph1_react_power,
                                                                    mesure->ph2_react_power,
                                                                    mesure->ph3_react_power,
                                                                    mesure->ph1_power_fact,
                                                                    mesure->ph2_power_fact,
                                                                    mesure->ph3_power_fact,         
                                                                    mesure->ph1_phase_angl,
                                                                    mesure->ph2_phase_angl,
                                                                    mesure->ph3_phase_angl,
                                                                    mesure->avg_line_voltage,
                                                                    mesure->avg_line_current,
                                                                    mesure->sum_line_current,
                                                                    mesure->tot_sys_power,
                                                                    mesure->tot_sys_volt_amp,
                                                                    mesure->tot_sys_react_pow,
                                                                    mesure->tot_sys_pow_fact,
                                                                    mesure->tot_sys_phase_ang,
                                                                    mesure->freq_supply_volt,
                                                                    mesure->imp_wh_last_reset,
                                                                    mesure->exp_wh_last_reset,
                                                                    mesure->imp_varh_last_reset,
                                                                    mesure->exp_varh_last_reset,
                                                                    mesure->vah_last_reset,
                                                                    mesure->va_last_reset,
                                                                    mesure->tot_sys_pow_demand,
                                                                    mesure->max_sys_pow_demand,
                                                                    mesure->tot_sys_va_demand,
                                                                    mesure->max_sys_va_demand,
                                                                    mesure->neutr_curr_demand,
                                                                    mesure->max_neutr_curr_demand,
                                                                    mesure->line_1_to_2_voltage,
                                                                    mesure->line_2_to_3_voltage,
                                                                    mesure->line_3_to_1_voltage,
                                                                    mesure->avg_line_to_line_volt,
                                                                    mesure->neutral_current,
                                                                    mesure->ph1_ln_volt_thd,
                                                                    mesure->ph2_ln_volt_thd,
                                                                    mesure->ph3_ln_volt_thd,
                                                                    mesure->ph1_current_thd,
                                                                    mesure->ph2_current_thd,
                                                                    mesure->ph3_current_thd,
                                                                    mesure->avg_line_neutr_volt,
                                                                    mesure->avg_line_current_thd,
                                                                    mesure->ph1_current_demand,
                                                                    mesure->ph2_current_demand,
                                                                    mesure->ph3_current_demand,
                                                                    mesure->ph1_max_curr_demand,
                                                                    mesure->ph2_max_curr_demand,
                                                                    mesure->ph3_max_curr_demand,
                                                                    mesure->line_1_to_2_volt_thd,
                                                                    mesure->line_2_to_3_volt_thd,
                                                                    mesure->line_3_to_1_volt_thd,
                                                                    mesure->avg_line_to_line_volt_thd,
                                                                    mesure->total_khw,
                                                                    mesure->total_kvarh
    );

    pMesureFile = fopen(mesureFilename, "a");

    if (pMesureFile == NULL)
    {
        printf("Unable to open file.\n");
        exit(EXIT_FAILURE);   
    }

    fputs(mesureRow, pMesureFile);
    fflush(pMesureFile);
    fclose(pMesureFile);
}            
