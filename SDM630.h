#ifndef __SDM630_H
#define __SDM630_H

#include <stdint.h>


typedef struct
{
    float ph1_voltage;	                // REG_ADDR=30001	Phase 1 line to neutral volts [Volts]
    float ph2_voltage;	                // REG_ADDR=30003	Phase 2 line to neutral volts [Volts]
    float ph3_voltage;	                // REG_ADDR=30005	Phase 3 line to neutral volts [Volts]

    float ph1_current;	                // REG_ADDR=30007	Phase 1 current [Amps]
    float ph2_current;	                // REG_ADDR=30009	Phase 2 current [Amps]
    float ph3_current;	                // REG_ADDR=30011	Phase 3 current [Amps]

    float ph1_power;	                // REG_ADDR=30013	Phase 1 power [Watts]
    float ph2_power;	                // REG_ADDR=30015	Phase 2 power [Watts]
    float ph3_power;	                // REG_ADDR=30017	Phase 3 power [Watts]

    float ph1_volt_amp;	                // REG_ADDR=30019	Phase 1 volt amps [VA]
    float ph2_volt_amp;	                // REG_ADDR=30021	Phase 2 volt amps [VA]
    float ph3_volt_amp;	                // REG_ADDR=30023	Phase 3 volt amps [VA]

    float ph1_react_power;	            // REG_ADDR=30025	Phase 1 volt amps reactive [VAr]
    float ph2_react_power;	            // REG_ADDR=30027	Phase 2 volt amps reactive [VAr]
    float ph3_react_power;	            // REG_ADDR=30029	Phase 3 volt amps reactive [VAr]

    float ph1_power_fact;	            // REG_ADDR=30031	Phase 1 power factor (1) [None]
    float ph2_power_fact;	            // REG_ADDR=30033	Phase 2 power factor (1) [None]
    float ph3_power_fact;	            // REG_ADDR=30035	Phase 3 power factor (1) [None]
            
    float ph1_phase_angl;	            // REG_ADDR=30037	Phase 1 phase angle [Degre es]
    float ph2_phase_angl;	            // REG_ADDR=30039	Phase 2 phase angle [Degre es]
    float ph3_phase_angl;	            // REG_ADDR=30041	Phase 3 phase angle [Degre es]

    float avg_line_voltage;	            // REG_ADDR=30043	Average line to neutral volts [Volts]
    float avg_line_current;	            // REG_ADDR=30047	Average line current [Amps]
    float sum_line_current;	            // REG_ADDR=30049	Sum of line currents [Amps]
    float tot_sys_power;	            // REG_ADDR=30053	Total system power [Watts]
    float tot_sys_volt_amp;	            // REG_ADDR=30057	Total system volt amps [VA]   
    float tot_sys_react_pow;	        // REG_ADDR=30061	Total system VAr [VAr]     
    float tot_sys_pow_fact;	            // REG_ADDR=30063	Total system power factor (1) [None] 
    float tot_sys_phase_ang;	        // REG_ADDR=30067	Total system phase angle [Degre es]
    float freq_supply_volt;	            // REG_ADDR=30071	Frequency of supply voltages [Hz]
    float imp_wh_last_reset;            // REG_ADDR=30073	Import Wh since last reset (2) [kWh/M Wh]
    float exp_wh_last_reset;	        // REG_ADDR=30075	Export Wh since last reset (2) [kWH/ MWh]
    float imp_varh_last_reset;	        // REG_ADDR=30077	Import VArh since last reset (2) [kVArh/MVArh]
    float exp_varh_last_reset;	        // REG_ADDR=30079	Export VArh since last reset (2) [kVArh/MVArh]
    float vah_last_reset;	            // REG_ADDR=30081	VAh since last reset (2) [kVAh/MVAh]
    float va_last_reset;	            // REG_ADDR=30083	Ah since last reset(3) [Ah/kAh]
    float tot_sys_pow_demand;	        // REG_ADDR=30085	Total system power demand [W]   
    float max_sys_pow_demand;	        // REG_ADDR=30087	Maximum total system power demand [VA]     
    float tot_sys_va_demand;	        // REG_ADDR=30101	Total system VA demand [VA
    float max_sys_va_demand;	        // REG_ADDR=30103	Maximum total  system VA demand [VA]
    float neutr_curr_demand;	        // REG_ADDR=30105	Neutral current demand [Amps] 
    float max_neutr_curr_demand;	    // REG_ADDR=30107	Maximum  neutral  current demand [Amps]

    float line_1_to_2_voltage;	        // REG_ADDR=30201	Line 1 to Line 2 volts [Volts]
    float line_2_to_3_voltage;	        // REG_ADDR=30203	Line 2 to Line 3 volts [Volts]
    float line_3_to_1_voltage;	        // REG_ADDR=30205	Line 3 to Line 1 volts [Volts]

    float avg_line_to_line_volt;	    // REG_ADDR=30207	Average line to line volts [Volts] 
    float neutral_current;	            // REG_ADDR=30225	Neutral current [Amps]

    float ph1_ln_volt_thd;	            // REG_ADDR=30235	Phase 1 L/N volts THD [%]
    float ph2_ln_volt_thd;	            // REG_ADDR=30237	Phase 2 L/N volts THD [%]
    float ph3_ln_volt_thd;	            // REG_ADDR=30239	Phase 3 L/N volts THD [%]

    float ph1_current_thd;	            // REG_ADDR=30241	Phase 1 Current THD [%]
    float ph2_current_thd;	            // REG_ADDR=30243	Phase 2 Current THD [%]
    float ph3_current_thd;	            // REG_ADDR=30245	Phase 3 Current THD [%] 

    float avg_line_neutr_volt;	        // REG_ADDR=30249	Average line to neutral volts THD [%]
    float avg_line_current_thd;	        // REG_ADDR=30251	Average line current THD [%]

    float ph1_current_demand;	        // REG_ADDR=30259	Phase 1 current demand [Amps]
    float ph2_current_demand;	        // REG_ADDR=30261	Phase 2 current demand [Amps]
    float ph3_current_demand;	        // REG_ADDR=30263	Phase 3 current demand [Amps]

    float ph1_max_curr_demand;	        // REG_ADDR=30265	Maximum phase  1  current demand [Amps]
    float ph2_max_curr_demand;	        // REG_ADDR=30267	Maximum phase  2  current demand [Amps]
    float ph3_max_curr_demand;	        // REG_ADDR=30269	Maximum phase  3  current demand [Amps]

    float line_1_to_2_volt_thd;	        // REG_ADDR=30335	Line 1 to line 2 volts THD [％]
    float line_2_to_3_volt_thd;	        // REG_ADDR=30337	Line 2 to line 3 volts THD [％]
    float line_3_to_1_volt_thd;	        // REG_ADDR=30339	Line 3 to line 1 volts THD [％]

    float avg_line_to_line_volt_thd ;   // REG_ADDR=30341	Average line to line volts THD [％]
    float total_khw;                    // REG_ADDR=30343	Total kwh [kwh]
    float total_kvarh;                  // REG_ADDR=30345	Total kvarh [kvarh]

}mesure_t;








static uint8_t reqFrame[12];
static uint8_t resFrame[200];

void parseReqFrameTCPModbus(uint8_t slaveId, uint8_t highByte, uint8_t lowByte, uint8_t);
float bytesToFloat(uint8_t * bytes);
//float bytesToFloat(uint8_t * frame, uint8_t highByte, uint8_t lowByte, uint8_t offset);
void readMesure(int client_fd, mesure_t * mesure);

void printBytesArray(uint8_t * arr, uint8_t size);

#endif //__SDM630_H